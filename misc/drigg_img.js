// $Id$
var dimgFetchImageHandler = function() {
  if ($("#edit-drigg-url-tmp").attr("value") == this.value || this.value.length == 0) {
	return;
  }
  $("#edit-drigg-url-tmp").attr("value", this.value);
  if ($("#edit-drigg-node-form").length > 0) {
	var url = '/drigg-img/js?url='+this.value;
	$("div.drigg-img-select").replaceWith('<div id="drigg-img-selection-loading"></div>');
	$("#drigg-img-selection-loading").show();
	$("#edit-drigg-img-get").show();
	
	var output = '';
	$.getJSON(url, function(data){
		$.each(data, function(i, item) {
				 output += '<div class="form-item" id="edit-drigg-img-'+item.src+'-wrapper">';
				 output += '<label class="option" for="'+item.src+'">';
				 output += '<input type="radio" id="edit-drigg-img-'+item.src+'" name="drigg_img" value="'+item.src+'" class="form-radio drigg-img-select" />';
				 output += '<img src="'+item.src+'" alt="'+item.src+'" title="'+item.src+'" /></label>';
				 output += '</div>';
			 }
		);
		$("#drigg-img-selection-loading").replaceWith('<div class="form-radios drigg-img-select"></div>');
		$(output).appendTo("div.drigg-img-select");
		
	    // UI handler
		$("div.drigg-img-select").ready(function(){
			dimgRadioButtonHandler();
			dimgImageClickHandler();
			$("#drigg-img-selection-loading").hide();
			$("#edit-drigg-img-get").hide();
		});
    });
  }
}

function dimgRadioButtonHandler() {
  //$('#drigg-img-selection-loading').show();
  // handle radio buttons
  $('div.drigg-img-select input.form-radio').hide();
  $('div.drigg-img-select img').hover(
    function(){
      $(this).addClass("dimg-hover");
    },
    function(){
      $(this).removeClass("dimg-hover");
    }
  );
}

function dimgImageClickHandler() {
  $('div.drigg-img-select img').bind("click", function(){
    $("div.drigg-img-select img.dimg-select").each(function(){
      $(this).removeClass("dimg-select");
      $(this).parent().children("input").attr("checked", "");
    });
    $(this).addClass("dimg-select");
    $(this).parent().children("input").attr("checked", "checked");
  });
}

$(document).ready(function () {
    // handle radio buttons
	dimgRadioButtonHandler();

    // handle image selection
	dimgImageClickHandler();
	
	$("#edit-url").blur(dimgFetchImageHandler);
	$("#edit-drigg-img-get").hide();
});

